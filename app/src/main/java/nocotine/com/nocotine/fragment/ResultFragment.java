package nocotine.com.nocotine.fragment;

import android.app.Fragment;
import android.graphics.ComposePathEffect;
import android.graphics.CornerPathEffect;
import android.graphics.PathDashPathEffect;
import android.graphics.PathEffect;
import android.graphics.SumPathEffect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;
import nocotine.com.nocotine.MainActivity;
import nocotine.com.nocotine.R;

/**
 * Created by ivanphytsyk on 3/2/17.
 */
public class ResultFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_results, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        LineChartView lineChartView = (LineChartView) view.findViewById(R.id.line_chart);

        List<PointValue> pointValues = new ArrayList<>();
        final List<Integer> cigarettesAmount = ((MainActivity) getActivity()).getLocalStorage().getSigaretteAmounts();

        int pointCount = cigarettesAmount.size() > 7 ? 7 : cigarettesAmount.size();

        for (int i = 0; i < pointCount; i++) {
            pointValues.add(new PointValue(i, cigarettesAmount.get(i + cigarettesAmount.size() - pointCount)));
        }

        Line line = new Line(pointValues).setColor(getResources().getColor(R.color.colorAccent)).setCubic(true);
        line.setShape(ValueShape.CIRCLE);
        line.setCubic(true);
        line.setFilled(true);
        line.setHasLabels(true);
        line.setHasLabelsOnlyForSelected(false);
        line.setHasLines(true);
        line.setHasPoints(true);
        line.setPathEffect(new CornerPathEffect(4));
        List<Line> lines = new ArrayList<>();
        lines.add(line);

        LineChartData lineChartData = new LineChartData();
        lineChartData.setLines(lines);

        lineChartView.setLineChartData(lineChartData);

        final Viewport v = new Viewport(lineChartView.getMaximumViewport());
        v.bottom = 4;
        lineChartView.setMaximumViewport(v);
        lineChartView.setCurrentViewport(v);
        lineChartView.setViewportCalculationEnabled(false);

        view.findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).showFragmentAfterCigarettesAmountInput(cigarettesAmount.get(cigarettesAmount.size() - 1));
            }
        });
    }
}
