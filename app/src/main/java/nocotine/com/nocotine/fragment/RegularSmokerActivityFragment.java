package nocotine.com.nocotine.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import nocotine.com.nocotine.MainActivity;
import nocotine.com.nocotine.R;

/**
 * Created by ivanphytsyk on 3/1/17.
 */

public class RegularSmokerActivityFragment extends Fragment {
    public static final String ARG_IS_CHAIN_SMOKER = "arg_is_chain";
    private boolean isChain;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isChain = getArguments().getBoolean(ARG_IS_CHAIN_SMOKER, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_regular_smoker_activities, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        final RadioButton activity1 = (RadioButton) view.findViewById(R.id.regular_smoker_activity_1);
        final RadioButton activity2 = (RadioButton) view.findViewById(R.id.regular_smoker_activity_2);
        final RadioButton activity3 = (RadioButton) view.findViewById(R.id.regular_smoker_activity_3);

        view.findViewById(R.id.confirm_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity mainActivity = (MainActivity) getActivity();
                if (activity1.isChecked()) {
                    mainActivity.showRegularSmokeConfirmFragment(activity1.getText().toString(), isChain);
                } else if (activity2.isChecked()) {
                    mainActivity.showRegularSmokeConfirmFragment(activity2.getText().toString(), isChain);
                } else if (activity3.isChecked()) {
                    mainActivity.showRegularSmokeConfirmFragment(activity3.getText().toString(), isChain);
                }
            }
        });

        TextView title = (TextView) view.findViewById(R.id.title);
        if (isChain) {
            title.setText(R.string.chain_smoker_title);
            String activity1Text = activity1.getText().toString().replace("10", "20");
            activity1.setText(activity1Text);
            String activity3Text = activity3.getText().toString().replace("20", "40");
            activity3.setText(activity3Text);
        } else {
            title.setText(R.string.regular_smoker_title);
        }
    }
}
