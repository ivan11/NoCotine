package nocotine.com.nocotine.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.xml.sax.XMLReader;

import nocotine.com.nocotine.MainActivity;
import nocotine.com.nocotine.R;

/**
 * Created by ivanphytsyk on 3/1/17.
 */

public class SocialSmokeFragmentTip1 extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_social_smoker_1, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        view.findViewById(R.id.next_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).showSocialSmokerFragmentTip2();
            }
        });

        TextView tips = (TextView) view.findViewById(R.id.tip);
        String tipsText = getString(R.string.social_smoker_tip);
        tips.setText(Html.fromHtml(tipsText, null, new LiTagHandler()));
    }
}
