package nocotine.com.nocotine.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import nocotine.com.nocotine.MainActivity;
import nocotine.com.nocotine.R;

/**
 * Created by ivanphytsyk on 3/1/17.
 */

public class RegularSmokerTipsFragment extends Fragment {

    public static final String EXTRA_IS_CHAIN_SMOKER = "extra_is_chain";

    private boolean isChain;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isChain = getArguments().getBoolean(EXTRA_IS_CHAIN_SMOKER, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_regular_smoker_tips, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        view.findViewById(R.id.next_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).showRegularSmokerActivityFragment(isChain);
            }
        });

        TextView tips = (TextView) view.findViewById(R.id.tip);
        String tipsText = getString(R.string.regular_smoker_tip_1);
        tips.setText(Html.fromHtml(tipsText, null, new LiTagHandler()));

        TextView title = (TextView) view.findViewById(R.id.title);
        if (isChain) {
            title.setText(R.string.chain_smoker_title);
        } else {
            title.setText(R.string.regular_smoker_title);
        }
    }
}
