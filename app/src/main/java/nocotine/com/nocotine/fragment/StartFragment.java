package nocotine.com.nocotine.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import nocotine.com.nocotine.MainActivity;
import nocotine.com.nocotine.R;

/**
 * Created by ivanphytsyk on 3/1/17.
 */

public class StartFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_start, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        final EditText name = (EditText) view.findViewById(R.id.name);
        final EditText age = (EditText) view.findViewById(R.id.age);

        view.findViewById(R.id.button_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameString = name.getText().toString();
                String ageString = age.getText().toString();
                if (!TextUtils.isEmpty(nameString) && !TextUtils.isEmpty(ageString)) {
                    int ageAmount = Integer.parseInt(ageString);
                    MainActivity mainActivity = (MainActivity) getActivity();
                    mainActivity.showWelcomeFragment();
                    mainActivity.getLocalStorage().saveUserName(nameString);
                    mainActivity.getLocalStorage().saveAge(ageAmount);
                }
            }
        });
    }
}
