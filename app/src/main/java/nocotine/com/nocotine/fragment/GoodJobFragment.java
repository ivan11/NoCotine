package nocotine.com.nocotine.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import nocotine.com.nocotine.MainActivity;
import nocotine.com.nocotine.R;

/**
 * Created by ivanphytsyk on 3/2/17.
 */
public class GoodJobFragment extends Fragment {
    public static final String ARG_FROM_AMOUNT = "arg_from_amount";
    public static final String ARG_TO_AMOUNT = "arg_to_amount";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_good_job, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        String fromAmount = getArguments().getString(ARG_FROM_AMOUNT);
        String toAmount = getArguments().getString(ARG_TO_AMOUNT);

        TextView tip = (TextView) view.findViewById(R.id.tip1);
        String tipText = getString(R.string.you_have_sown_smoking);
        tipText = tipText.replace("{from}", fromAmount).replace("{to}", toAmount);
        tip.setText(tipText);

        view.findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).showRegularSmokerTipsFragment();
            }
        });

        view.findViewById(R.id.result_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (  (MainActivity)getActivity()).showResultFragment();
            }
        });
    }
}


