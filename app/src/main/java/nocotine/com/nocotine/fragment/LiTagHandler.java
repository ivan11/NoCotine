package nocotine.com.nocotine.fragment;

import android.text.Editable;
import android.text.Html;

import org.xml.sax.XMLReader;

/**
 * Created by ivanphytsyk on 3/1/17.
 */

public class LiTagHandler implements Html.TagHandler {
    boolean first = true;
    @Override
    public void handleTag(boolean b, String tag, Editable output, XMLReader xmlReader) {
        if (tag.equals("li")) {
            char lastChar = 0;
            if (output.length() > 0)
                lastChar = output.charAt(output.length() - 1);
            if (first) {
                if (lastChar == '\n')
                    output.append("\t•  ");
                else
                    output.append("\n\t•  ");
                first = false;
            } else {
                first = true;
            }
        }
    }
}
