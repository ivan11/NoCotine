package nocotine.com.nocotine.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import nocotine.com.nocotine.MainActivity;
import nocotine.com.nocotine.R;

/**
 * Created by ivanphytsyk on 3/1/17.
 */

public class HowManySigarettesFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_how_many_cigarettes, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        final EditText sigarettesAmount = (EditText) view.findViewById(R.id.sigarettes_amount);

        view.findViewById(R.id.confirm_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int amount = Integer.parseInt(sigarettesAmount.getText().toString());
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.getLocalStorage().saveSigaretteAmount(amount);
                mainActivity.showFragmentAfterCigarettesAmountInput(amount);
            }
        });
    }
}
