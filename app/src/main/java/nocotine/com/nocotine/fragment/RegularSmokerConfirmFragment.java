package nocotine.com.nocotine.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import nocotine.com.nocotine.MainActivity;
import nocotine.com.nocotine.R;

/**
 * Created by ivanphytsyk on 3/1/17.
 */

public class RegularSmokerConfirmFragment extends Fragment {
    public static final String ARG_SELECTED_TASK = "arg_selected_task";

    public static final String ARG_IS_CHAIN_SMOKER = "arg_is_chain";
    private boolean isChain;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isChain = getArguments().getBoolean(ARG_IS_CHAIN_SMOKER, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_regular_smoker_confirm, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        view.findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).showProgressFragment();
            }
        });
        view.findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).showMissedStemFragment();
            }
        });
        TextView selectedTask = (TextView) view.findViewById(R.id.selected_task);
        String text = getArguments().getString(ARG_SELECTED_TASK);
        selectedTask.setText(text);

        TextView title = (TextView) view.findViewById(R.id.title);
        if (isChain) {
            title.setText(R.string.chain_smoker_title);
        } else {
            title.setText(R.string.regular_smoker_title);
        }
    }
}
