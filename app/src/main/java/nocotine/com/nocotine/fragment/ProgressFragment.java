package nocotine.com.nocotine.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.List;

import nocotine.com.nocotine.MainActivity;
import nocotine.com.nocotine.R;

/**
 * Created by ivanphytsyk on 3/2/17.
 */
public class ProgressFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_progress, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        final EditText progressText = (EditText) view.findViewById(R.id.sigarettes_amount);

        view.findViewById(R.id.confirm_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = progressText.getText().toString();
                if (!TextUtils.isEmpty(text)) {
                    int newCigarettesAmount = Integer.parseInt(text);
                    MainActivity mainActivity = (MainActivity) getActivity();
                    List<Integer> previousAmounts = mainActivity.getLocalStorage().getSigaretteAmounts();
                    int firstCigarettesAmount = previousAmounts.get(0);
                    mainActivity.getLocalStorage().saveSigaretteAmount(newCigarettesAmount);

                    if (newCigarettesAmount < firstCigarettesAmount) {
                        mainActivity.showGoodJobFragment(String.valueOf(firstCigarettesAmount), String.valueOf(newCigarettesAmount));
                    } else {
                        mainActivity.showTryHarderFragment();
                    }
                }
            }
        });
    }
}
