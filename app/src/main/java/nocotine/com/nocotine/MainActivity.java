package nocotine.com.nocotine;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import nocotine.com.nocotine.fragment.GoodJobFragment;
import nocotine.com.nocotine.fragment.HowManySigarettesFragment;
import nocotine.com.nocotine.fragment.MissedStepFragment;
import nocotine.com.nocotine.fragment.ProgressFragment;
import nocotine.com.nocotine.fragment.RegularSmokerActivityFragment;
import nocotine.com.nocotine.fragment.RegularSmokerConfirmFragment;
import nocotine.com.nocotine.fragment.RegularSmokerTipsFragment;
import nocotine.com.nocotine.fragment.ResultFragment;
import nocotine.com.nocotine.fragment.SocialSmokeFragmentTip1;
import nocotine.com.nocotine.fragment.SocialSmokeFragmentTip2;
import nocotine.com.nocotine.fragment.StartFragment;
import nocotine.com.nocotine.fragment.ThanksFragment;
import nocotine.com.nocotine.fragment.TryHarderFragment;
import nocotine.com.nocotine.fragment.WelcomeFragment;

public class MainActivity extends AppCompatActivity {

    private LocalStorage localStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        localStorage = new LocalStorage(this);
        showFirstScreen();
    }

    private void showFirstScreen() {
        List<Integer> cigarettesAmount = localStorage.getSigaretteAmounts();
        String userName = localStorage.getUserName();
        if (TextUtils.isEmpty(userName)) {
            showStartFragment();
        } else {
            if (!cigarettesAmount.isEmpty()) {
                int lastSavedAmount = cigarettesAmount.get(0);
                showFragmentAfterCigarettesAmountInput(lastSavedAmount, false);
            } else {
                showHowManySigarettesFragment();
            }
        }
    }

    /**
     *
     * @param amount - first sigarettes amount
     */
    public void showFragmentAfterCigarettesAmountInput(int amount) {
        showFragmentAfterCigarettesAmountInput(amount, true);
    }

    /**
     * After user add his initial cigs amount we navigate him into some screen
     * @param amount
     * @param addToBackStack - if we can get back by clicking "back" button
     */
    public void showFragmentAfterCigarettesAmountInput(int amount, boolean addToBackStack) {
        if (amount <= 5) {
            showSocialSmokerFragmentTip1(addToBackStack);
        } else if (amount > 5 && amount <= 14) {
            showRegularSmokerTipsFragment(addToBackStack);
        } else {
            showChainSmokerFragment(addToBackStack);
        }
    }

    /**
     *
     * @param addToBackStack
     */
    private void showChainSmokerFragment(boolean addToBackStack) {
        Fragment fragment = new RegularSmokerTipsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(RegularSmokerTipsFragment.EXTRA_IS_CHAIN_SMOKER, true);
        fragment.setArguments(bundle);
        showFragment(fragment, addToBackStack);
    }

    /**
     *
     * @param addToBackStack if we can get back by clicking "Back" button
     */
    private void showRegularSmokerTipsFragment(boolean addToBackStack) {
        Fragment fragment = new RegularSmokerTipsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(RegularSmokerTipsFragment.EXTRA_IS_CHAIN_SMOKER, false);
        fragment.setArguments(bundle);
        showFragment(fragment, addToBackStack);
    }

    public void showChainSmokerFragment() {
       showChainSmokerFragment(true);
    }

    public void showRegularSmokerTipsFragment() {
        showRegularSmokerTipsFragment(true);
    }

    public void showSocialSmokerFragmentTip1(boolean addToBackStack) {
        showFragment(new SocialSmokeFragmentTip1(), addToBackStack);
    }

    public void showWelcomeFragment() {
        showFragment(new WelcomeFragment());
    }

    /**
     *
     * @return localStorage @see LocalStorate
     */
    public LocalStorage getLocalStorage() {
        return localStorage;
    }

    public void showHowManySigarettesFragment() {
        showFragment(new HowManySigarettesFragment(), false);
    }

    private void showFragment(Fragment fragment) {
        showFragment(fragment, true);
    }

    /**
     *
     * @param fragment - fragment we want to show
     * @param addToBackStack - if it can be available to get back through "back" button
     */
    private void showFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.slide_in_right, R.animator.slide_out_left, R.animator.slide_in_left, R.animator.slide_out_right)
                .replace(R.id.container, fragment);
        if (addToBackStack) {
            transaction.addToBackStack(fragment.getClass().getName());
        }
        transaction.commit();
    }

    public void showSocialSmokerFragmentTip2() {
        showFragment(new SocialSmokeFragmentTip2());
    }

    public void showThanksFragment() {
        showFragment(new ThanksFragment());
    }

    /**
     *
     * @param isChain - if user is chainSmoker
     */
    public void showRegularSmokerActivityFragment(boolean isChain) {
        Fragment fragment = new RegularSmokerActivityFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(RegularSmokerActivityFragment.ARG_IS_CHAIN_SMOKER, isChain);
        fragment.setArguments(bundle);
        showFragment(fragment);
    }

    /**
     * Shows Confirm task screen
     * @param selectedTask - task user selecte on step before
     * @param isChain if user is chainSmoker
     */
    public void showRegularSmokeConfirmFragment(String selectedTask, boolean isChain) {
        Fragment fragment = new RegularSmokerConfirmFragment();
        Bundle bundle = new Bundle();
        bundle.putString(RegularSmokerConfirmFragment.ARG_SELECTED_TASK, selectedTask);
        bundle.putBoolean(RegularSmokerActivityFragment.ARG_IS_CHAIN_SMOKER, isChain);
        fragment.setArguments(bundle);
        showFragment(fragment);
    }

    public void showStartFragment() {
        showFragment(new StartFragment());
    }

    public void showMissedStemFragment() {
        showFragment(new MissedStepFragment());
    }

    public void showProgressFragment() {
        showFragment(new ProgressFragment());
    }

    /**
     * Shows Good jog screen
     * @param fromAmount - initial cigs amount
     * @param toAmount - new cigs amount
     */
    public void showGoodJobFragment(String fromAmount, String toAmount) {
        Fragment fragment = new GoodJobFragment();
        Bundle bundle = new Bundle();
        bundle.putString(GoodJobFragment.ARG_FROM_AMOUNT, fromAmount);
        bundle.putString(GoodJobFragment.ARG_TO_AMOUNT, toAmount);
        fragment.setArguments(bundle);
        showFragment(fragment);
    }

    public void showTryHarderFragment() {
        showFragment(new TryHarderFragment());
    }

    public void showResultFragment() {
        showFragment(new ResultFragment());
    }
}
