package nocotine.com.nocotine;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ivanphytsyk on 3/1/17.
 */

public class LocalStorage {
    private static final String KEY_SIGARETTE_AMOUNT = "key_sigarette_amount";
    private static final String KEY_AGE= "key_age";
    private static final String KEY_NAME= "key_name";
    private SharedPreferences sharedPreferences;

    public LocalStorage(Context context) {
        this.sharedPreferences = context.getSharedPreferences("Nocotine", Context.MODE_PRIVATE);
    }

    public void saveAge(int age) {
        sharedPreferences.edit().putInt(KEY_AGE, age).apply();
    }

    /**
     * Save new user sigs amount
     * @param amount
     */
    public void saveSigaretteAmount(int amount) {
        String previousAmountsString = sharedPreferences.getString(KEY_SIGARETTE_AMOUNT, "");
        if (!TextUtils.isEmpty(previousAmountsString)) {
            Type listType = new TypeToken<ArrayList<Integer>>(){}.getType();
            List<Integer> amounts = new Gson().fromJson(previousAmountsString, listType);
            amounts.add(amount);
            String newAmountsString = new Gson().toJson(amounts);
            sharedPreferences.edit().putString(KEY_SIGARETTE_AMOUNT, newAmountsString).apply();
        } else {
            List<Integer> newAmounts = new ArrayList<>();
            newAmounts.add(amount);
            String newAmountsString = new Gson().toJson(newAmounts);
            sharedPreferences.edit().putString(KEY_SIGARETTE_AMOUNT, newAmountsString).apply();
        }
    }

    /**
     * get Previous user's cigarettes notes
     * @return list of previous ones
     */
    public List<Integer> getSigaretteAmounts() {
        String previousAmountsString = sharedPreferences.getString(KEY_SIGARETTE_AMOUNT, "");
        if (!TextUtils.isEmpty(previousAmountsString)) {
            Type listType = new TypeToken<ArrayList<Integer>>(){}.getType();
            return new Gson().fromJson(previousAmountsString, listType);
        } else {
            return new ArrayList<>();
        }
    }

    /**
     *
     * @return current user name
     */
    public String getUserName() {
        return sharedPreferences.getString(KEY_NAME, "");
    }

    /**
     * Save user name
     * @param name
     */
    public void saveUserName(String name) {
        sharedPreferences.edit().putString(KEY_NAME, name).apply();
    }

}
